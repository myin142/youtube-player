import {
  Menu,
  BrowserWindow,
  MenuItemConstructorOptions,
} from 'electron';

interface DarwinMenuItemConstructorOptions extends MenuItemConstructorOptions {
  selector?: string;
  submenu?: DarwinMenuItemConstructorOptions[] | Menu;
}

export default class MenuBuilder {
  mainWindow: BrowserWindow;

  constructor(mainWindow: BrowserWindow) {
    this.mainWindow = mainWindow;
  }

  buildMenu(): Menu {
    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      this.setupDevelopmentEnvironment();
    }

    // const template =
    //   process.platform === 'darwin'
    //     ? this.buildDarwinTemplate()
    //     : this.buildDefaultTemplate();

    const menu = Menu.buildFromTemplate(this.buildDefaultTemplate());
    Menu.setApplicationMenu(menu);

    return menu;
  }

  setupDevelopmentEnvironment(): void {
    this.mainWindow.webContents.on('context-menu', (_, props) => {
      const { x, y } = props;

      Menu.buildFromTemplate([
        {
          label: 'Inspect element',
          click: () => {
            this.mainWindow.webContents.inspectElement(x, y);
          },
        },
      ]).popup({ window: this.mainWindow });
    });
  }

  buildDarwinTemplate(): MenuItemConstructorOptions[] {
    // const subMenuAbout: DarwinMenuItemConstructorOptions = {
    //   label: 'Electron',
    //   submenu: [
    //     {
    //       label: 'About ElectronReact',
    //       selector: 'orderFrontStandardAboutPanel:',
    //     },
    //     { type: 'separator' },
    //     { label: 'Services', submenu: [] },
    //     { type: 'separator' },
    //     {
    //       label: 'Hide ElectronReact',
    //       accelerator: 'Command+H',
    //       selector: 'hide:',
    //     },
    //     {
    //       label: 'Hide Others',
    //       accelerator: 'Command+Shift+H',
    //       selector: 'hideOtherApplications:',
    //     },
    //     { label: 'Show All', selector: 'unhideAllApplications:' },
    //     { type: 'separator' },
    //     {
    //       label: 'Quit',
    //       accelerator: 'Command+Q',
    //       click: () => {
    //         app.quit();
    //       },
    //     },
    //   ],
    // };
    // const subMenuEdit: DarwinMenuItemConstructorOptions = {
    //   label: 'Edit',
    //   submenu: [
    //     { label: 'Undo', accelerator: 'Command+Z', selector: 'undo:' },
    //     { label: 'Redo', accelerator: 'Shift+Command+Z', selector: 'redo:' },
    //     { type: 'separator' },
    //     { label: 'Cut', accelerator: 'Command+X', selector: 'cut:' },
    //     { label: 'Copy', accelerator: 'Command+C', selector: 'copy:' },
    //     { label: 'Paste', accelerator: 'Command+V', selector: 'paste:' },
    //     {
    //       label: 'Select All',
    //       accelerator: 'Command+A',
    //       selector: 'selectAll:',
    //     },
    //   ],
    // };
    // const subMenuViewDev: MenuItemConstructorOptions = {
    //   label: 'View',
    //   submenu: [
    //     {
    //       label: 'Reload',
    //       accelerator: 'Command+R',
    //       click: () => {
    //         this.mainWindow.webContents.reload();
    //       },
    //     },
    //     {
    //       label: 'Toggle Full Screen',
    //       accelerator: 'Ctrl+Command+F',
    //       click: () => {
    //         this.mainWindow.setFullScreen(!this.mainWindow.isFullScreen());
    //       },
    //     },
    //     {
    //       label: 'Toggle Developer Tools',
    //       accelerator: 'Alt+Command+I',
    //       click: () => {
    //         this.mainWindow.webContents.toggleDevTools();
    //       },
    //     },
    //   ],
    // };
    // const subMenuViewProd: MenuItemConstructorOptions = {
    //   label: 'View',
    //   submenu: [
    //     {
    //       label: 'Toggle Full Screen',
    //       accelerator: 'Ctrl+Command+F',
    //       click: () => {
    //         this.mainWindow.setFullScreen(!this.mainWindow.isFullScreen());
    //       },
    //     },
    //   ],
    // };
    // const subMenuWindow: DarwinMenuItemConstructorOptions = {
    //   label: 'Window',
    //   submenu: [
    //     {
    //       label: 'Minimize',
    //       accelerator: 'Command+M',
    //       selector: 'performMiniaturize:',
    //     },
    //     { label: 'Close', accelerator: 'Command+W', selector: 'performClose:' },
    //     { type: 'separator' },
    //     { label: 'Bring All to Front', selector: 'arrangeInFront:' },
    //   ],
    // };
    // const subMenuHelp: MenuItemConstructorOptions = {
    //   label: 'Help',
    //   submenu: [
    //     {
    //       label: 'Learn More',
    //       click() {
    //         shell.openExternal('https://electronjs.org');
    //       },
    //     },
    //     {
    //       label: 'Documentation',
    //       click() {
    //         shell.openExternal(
    //           'https://github.com/electron/electron/tree/master/docs#readme'
    //         );
    //       },
    //     },
    //     {
    //       label: 'Community Discussions',
    //       click() {
    //         shell.openExternal('https://www.electronjs.org/community');
    //       },
    //     },
    //     {
    //       label: 'Search Issues',
    //       click() {
    //         shell.openExternal('https://github.com/electron/electron/issues');
    //       },
    //     },
    //   ],
    // };

    // const subMenuView =
    //   process.env.NODE_ENV === 'development' ||
    //   process.env.DEBUG_PROD === 'true'
    //     ? subMenuViewDev
    //     : subMenuViewProd;

    // return [subMenuAbout, subMenuEdit, subMenuView, subMenuWindow, subMenuHelp];
    return [];
  }

  private static isDebug(): boolean {
    return (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    );
  }

  buildDefaultTemplate() {
    // const viewMenus = [
    //   {
    //     label: 'Toggle &Full Screen',
    //     accelerator: 'F11',
    //     click: () => {
    //       this.mainWindow.setFullScreen(!this.mainWindow.isFullScreen());
    //     },
    //   },
    // ];
    // if (MenuBuilder.isDebug()) {
    //   const viewMenuDebug = [
    //     {
    //       label: '&Reload',
    //       accelerator: 'Ctrl+R',
    //       click: () => {
    //         this.mainWindow.webContents.reload();
    //       },
    //     },
    //     {
    //       label: 'Toggle &Developer Tools',
    //       accelerator: 'Alt+Ctrl+I',
    //       click: () => {
    //         this.mainWindow.webContents.toggleDevTools();
    //       },
    //     },
    //   ];
    //   viewMenus.push(...viewMenuDebug);
    // }
    const templateDefault: any = [
      // {
      //   label: '&Playlist',
      //   submenu: [
      //     {
      //       label: 'Migrate old playlist folder',
      //       click: async () => {
      //         const paths = await dialog.showOpenDialog({
      //           properties: ['openDirectory'],
      //         });

      //         if (!paths.canceled && paths.filePaths.length > 0) {
      //           const path = paths.filePaths[0];
      //           const service = new PlaylistService();

      //           const folders = await service.getLibraryFolderInfos(path);
      //           const playlists = folders.map(x => x.playlist).filter(x => !!x);
      //           playlists.forEach(p => {
      //             p.videos.forEach((v: any) => delete v.fileName); // delete old fileName property
      //           })
      //           service.savePlaylist(playlists, app.getPath('appData'));

      //           this.mainWindow.webContents.send(AppEvent.REDIRECT, '/');
      //         }
      //       },
      //     },
      //     {
      //       label: '&Close',
      //       accelerator: 'Ctrl+W',
      //       click: () => {
      //         this.mainWindow.close();
      //       },
      //     },
      //   ],
      // },
      // {
      //   label: '&View',
      //   submenu: [...viewMenus],
      // },
      // {
      //   label: 'Help',
      //   submenu: [
      //     {
      //       label: 'Learn More',
      //       click() {
      //         shell.openExternal('https://electronjs.org');
      //       },
      //     },
      //     {
      //       label: 'Documentation',
      //       click() {
      //         shell.openExternal(
      //           'https://github.com/electron/electron/tree/master/docs#readme'
      //         );
      //       },
      //     },
      //     {
      //       label: 'Community Discussions',
      //       click() {
      //         shell.openExternal('https://www.electronjs.org/community');
      //       },
      //     },
      //     {
      //       label: 'Search Issues',
      //       click() {
      //         shell.openExternal('https://github.com/electron/electron/issues');
      //       },
      //     },
      //   ],
      // },
    ];

    return templateDefault;
  }
}
